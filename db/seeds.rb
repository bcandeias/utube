# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


#### Users
User.delete_all
User.create(first_name: 'User', last_name: '1', email: 'user1@example.com', password: 'secret')
User.create(first_name: 'User', last_name: '2', email: 'user2@example.com', password: 'secret')
User.create(first_name: 'User', last_name: '3', email: 'user3@example.com', password: 'secret')
User.create(first_name: 'User', last_name: '4', email: 'user4@example.com', password: 'secret')
User.create(first_name: 'User', last_name: '5', email: 'user5@example.com', password: 'secret')
User.create(first_name: 'User', last_name: '6', email: 'user6@example.com', password: 'secret')
User.create(first_name: 'User', last_name: '7', email: 'user7@example.com', password: 'secret')
User.create(first_name: 'User', last_name: '8', email: 'user8@example.com', password: 'secret')
User.create(first_name: 'User', last_name: '9', email: 'user9@example.com', password: 'secret')
User.create(first_name: 'User', last_name: '10', email: 'user10@example.com', password: 'secret')
User.create(first_name: 'User', last_name: '11', email: 'user11@example.com', password: 'secret')
User.create(first_name: 'User', last_name: '12', email: 'user12@example.com', password: 'secret')
User.create(first_name: 'Bruno', last_name: 'Candeias', email: 'bcandeias@example.com', password: 'secret') # :id = 13

#### Channels
Channel.delete_all
Channel.create(user_id: '1')
Channel.create(user_id: '2')
Channel.create(user_id: '3')
Channel.create(user_id: '4')
Channel.create(user_id: '5')
Channel.create(user_id: '6')
Channel.create(user_id: '7')
Channel.create(user_id: '8')
Channel.create(user_id: '9')
Channel.create(user_id: '10')
Channel.create(user_id: '11')
Channel.create(user_id: '12')
Channel.create(user_id: '13')


#### Videos
Video.delete_all

# Videos for channel 1 (User 1)
Video.create(channel_id: '1', title: 'Doris Day - Let\'s Keep Smiling', embbeded: '<iframe width="640" height="390" src="//www.youtube.com/embed/5Myu32IV72o" frameborder="0" allowfullscreen></iframe>')
Video.create(channel_id: '1', title: 'Benny Goodman & His Orchestra - Sing Sing Sing ', embbeded: '<iframe width="640" height="390" src="//www.youtube.com/embed/Dl43Hzy5eVs" frameborder="0" allowfullscreen></iframe>')
Video.create(channel_id: '1', title: 'David Murray - Infinity Quartet live at Timisoara Jazz Festival', embbeded: '<iframe width="640" height="390" src="//www.youtube.com/embed/zdCQw323Muk" frameborder="0" allowfullscreen></iframe>')
Video.create(channel_id: '1', title: 'The Chick Webb Saga', embbeded: '<iframe width="640" height="390" src="//www.youtube.com/embed/NV6QXLJwgSU" frameborder="0" allowfullscreen></iframe>')
Video.create(channel_id: '1', title: 'Ray Charles - Georgia On My Mind', embbeded: '<iframe width="640" height="390" src="//www.youtube.com/embed/CEjeMG6YiTU" frameborder="0" allowfullscreen></iframe>')
Video.create(channel_id: '1', title: 'John Lee Hooker - Boom Boom', embbeded: '<iframe width="640" height="390" src="//www.youtube.com/embed/kkoo_nbWSi8" frameborder="0" allowfullscreen></iframe>')

# Videos for channel 2 (User 2)
Video.create(channel_id: '2', title: 'Mumford & Sons - I Will Wait', embbeded: '<iframe width="640" height="390" src="//www.youtube.com/embed/rGKfrgqWcv0" frameborder="0" allowfullscreen></iframe>')
Video.create(channel_id: '2', title: 'Mumford & Sons - The Cave', embbeded: '<iframe width="640" height="390" src="//www.youtube.com/embed/fNy8llTLvuA" frameborder="0" allowfullscreen></iframe>')
Video.create(channel_id: '2', title: 'Jack Johnson - Upside Down', embbeded: '<iframe width="640" height="390" src="//www.youtube.com/embed/dqUdI4AIDF0" frameborder="0" allowfullscreen></iframe>')
Video.create(channel_id: '2', title: 'Jack Johnson- Banana Pancakes', embbeded: '<iframe width="640" height="390" src="//www.youtube.com/embed/6Graa_Vm5eA" frameborder="0" allowfullscreen></iframe>')
Video.create(channel_id: '2', title: '"Sirens" (Official Music Video) - Pearl Jam', embbeded: '<iframe width="640" height="390" src="//www.youtube.com/embed/qQXP6TDtW0w" frameborder="0" allowfullscreen></iframe>')
Video.create(channel_id: '2', title: 'Foo Fighters - Everlong Live at Wembley Stadium 2008 HD', embbeded: '<iframe width="640" height="390" src="//www.youtube.com/embed/_KXYVv36-Lk" frameborder="0" allowfullscreen></iframe>')

# Videos for channel 3 (User 3)
Video.create(channel_id: '3', title: 'OCEAN - John Butler - 2012 Studio Version', embbeded: '<iframe width="640" height="390" src="//www.youtube.com/embed/jdYJf_ybyVo" frameborder="0" allowfullscreen></iframe>')
Video.create(channel_id: '3', title: 'Into the Wild - Rise (Eddie Vedder)', embbeded: '<iframe width="640" height="390" src="//www.youtube.com/embed/32Js2Ef5Ojg" frameborder="0" allowfullscreen></iframe>')
Video.create(channel_id: '3', title: 'Eddie Vedder - Society', embbeded: '<iframe width="640" height="390" src="//www.youtube.com/embed/Cy6iwP9Ux3A" frameborder="0" allowfullscreen></iframe>')

# Videos for channel 4 (User 4)
Video.create(channel_id: '4', title: 'Metallica - The Day That Never Comes[Video Premiere]', embbeded: '<iframe width="640" height="390" src="//www.youtube.com/embed/Mlahvvymkxc" frameborder="0" allowfullscreen></iframe>')
Video.create(channel_id: '4', title: 'Metallica - I Dissapear Music Video', embbeded: '<iframe width="640" height="390" src="//www.youtube.com/embed/hNpQpjyc9C8" frameborder="0" allowfullscreen></iframe>')
Video.create(channel_id: '4', title: 'Metallica - Whiskey In The Jar [Official Music Video]', embbeded: '<iframe width="640" height="390" src="//www.youtube.com/embed/boanuwUMNNQ" frameborder="0" allowfullscreen></iframe>')

# Videos for channel 5 (User 5)
Video.create(channel_id: '5', title: 'Gemini - And You', embbeded: '<iframe width="640" height="390" src="//www.youtube.com/embed/LY1FzvRVo3s" frameborder="0" allowfullscreen></iframe>')
Video.create(channel_id: '5', title: 'Feed Me - Blood Red', embbeded: '<iframe width="640" height="390" src="//www.youtube.com/embed/G6GIdGhxyHw" frameborder="0" allowfullscreen></iframe>')
Video.create(channel_id: '5', title: 'Nero - Must Be The Feeling (Delta Heavy Remix)', embbeded: '<iframe width="640" height="390" src="//www.youtube.com/embed/9W04IKR1E2M" frameborder="0" allowfullscreen></iframe>')

# Videos for channel 6 (User 6)
Video.create(channel_id: '6', title: 'Pharrell Williams - Happy (Official Music Video)', embbeded: '<iframe width="640" height="390" src="//www.youtube.com/embed/y6Sxv-sUYtM" frameborder="0" allowfullscreen></iframe>')
Video.create(channel_id: '6', title: '[Official Video] Daft Punk - Pentatonix', embbeded: '<iframe width="640" height="390" src="//www.youtube.com/embed/3MteSlpxCpo" frameborder="0" allowfullscreen></iframe>')
Video.create(channel_id: '6', title: 'Daft Punk - Get Lucky (Official Audio) ft. Pharrell Williams', embbeded: '<iframe width="640" height="390" src="//www.youtube.com/embed/5NV6Rdv1a3I" frameborder="0" allowfullscreen></iframe>')


#### Collections
Collection.delete_all

# Collections of User 1
Collection.create(user_id: '1', name: 'Collection 1_1')
Collection.create(user_id: '1', name: 'Collection 2_1')

# Collections of User 1
Collection.create(user_id: '2', name: 'Collection 1_2')
Collection.create(user_id: '2', name: 'Collection 2_2')
Collection.create(user_id: '2', name: 'Collection 3_2') # :id = 6


#### Subscriptions
Subscription.delete_all

#Subscriptions of User 1
Subscription.create(user_id: '1', channel_id:'2', collection_id: '1')
Subscription.create(user_id: '1', channel_id:'3', collection_id: '2')
Subscription.create(user_id: '1', channel_id:'4', collection_id: '2')
Subscription.create(user_id: '1', channel_id:'5')
Subscription.create(user_id: '1', channel_id:'6')   # :id = 6

#Subscriptions of User 2
Subscription.create(user_id: '2', channel_id:'1', collection_id: '3')
Subscription.create(user_id: '2', channel_id:'3', collection_id: '4')
Subscription.create(user_id: '2', channel_id:'4', collection_id: '4')
Subscription.create(user_id: '2', channel_id:'5')
Subscription.create(user_id: '2', channel_id:'6')