class User < ActiveRecord::Base

  # ASSOCIATIONS
  has_one :channel
  has_many :videos, through: :channel
  has_many :collections
  has_many :subscriptions


  # VALIDATIONS

  # DEFINITIONS

  def name
    "#{first_name} #{last_name}"
  end

  def self.authenticate(email='', password='')
    user = User.find_by_email(email)
    if user && user.password == password
      return user
    else
      return false
    end
  end

  def subscriptions_all
    Subscription.where('user_id = ?', id)
  end

  def subscriptions_videos
    subs = subscriptions_all
    videos = []
    subs.each do |s|
      videos = videos + s.videos
    end
    videos.sort!{|a,b| b.created_at <=> a.created_at}
  end

  def uploads
    channel.videos
  end


end
