class Subscription < ActiveRecord::Base

  # ASSOCIATIONS
  belongs_to :collection
  belongs_to :channel

  # VALIDATIONS

  # DEFINITIONS

  def channel_name
     channel.user.name
  end

  def videos
    channel.videos
  end
end
