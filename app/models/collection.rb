class Collection < ActiveRecord::Base

  # ASSOCIATIONS
  has_many :subscriptions
  belongs_to :user

  # VALIDATIONS

  # DEFINITIONS
  def videos
    videos = []
    subs = subscriptions
    subs.each do |subscription|
      v = subscription.channel.videos
      videos = videos + v
    end
    videos.sort!{|a,b| b.created_at <=> a.created_at}
  end
end
