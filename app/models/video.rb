class Video < ActiveRecord::Base

  # ASSOCIATIONS
  belongs_to :channel


  # VALIDATIONS

  # DEFINITIONS

  def owner
    channel.user
  end
end
