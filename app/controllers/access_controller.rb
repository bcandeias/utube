class AccessController < ApplicationController

  layout 'access'

  def login
    # Renders login
  end

  def attempt_login
    authorized_user = User.authenticate(params[:email], params[:password])
    if authorized_user
      session[:user_id] = authorized_user.id
      session[:email] = authorized_user.email
      session[:username] = authorized_user.name
      flash[:notice] = 'You are now logged in.'
      redirect_to( :controller => 'home', :action => 'home')
    else
      flash[:notice] = 'Invalid username/password combination.'
      redirect_to(:action => 'login')
    end
  end

  def logout
    session[:user_id] = nil
    session[:username] = nil
    flash[:notice] = 'You have been logged out.'
    redirect_to( :controller => 'home', :action => 'home')
  end
end
