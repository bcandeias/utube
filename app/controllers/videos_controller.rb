class VideosController < ApplicationController

  before_action :set_video, only: [:show]


  def show
    if session[:user_id]
      @user = User.find(session[:user_id])
    end
    # @video = Video.find(params[:id])    ---> set_video
  end




  ## Rails stuff... Not sure what to do about it.

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_video
      @video = Video.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def video_params
      params.require(:video).permit(:channel_id, :title, :embbeded)
    end



end
