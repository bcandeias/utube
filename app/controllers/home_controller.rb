class HomeController < ApplicationController

  def home
    if session[:user_id]
      @user = User.find(session[:user_id])
      @nonsubscribed = Subscription.where.not(user_id: @user.id ).limit(5)
    end

    #render 'home/home'
  end

end
