class CollectionsController < ApplicationController
  before_action :set_collection, only: [:show, :edit, :update, :destroy]


  def index
    @user = User.find(params[:user_id])
    @collections = User.find(params[:user_id]).collections
  end


  def show
    @user = User.find(params[:user_id])
    @collection = Collection.find(params[:id])
  end


  def new
    @user = User.find(params[:user_id])
    @collection = User.find(params[:user_id]).collections.new
  end


  def edit
    @user = User.find(params[:user_id])
  end


  def create
    @collection = @user.collections.new(collection_params)
      if @collection.save
        redirect_to user_collections_path(@user), notice: 'Collection was successfully created.'
      else
        render action: 'new'
      end
  end


  def update
    params[:collection][:subscription_ids] ||= []
    @user = User.find(params[:user_id])
      if @collection.update(collection_params)
        redirect_to user_collection_path(@user, @collection), notice: 'Collection was successfully updated.'
      else
        render action: 'edit'

      end
  end

  def destroy
    @user = User.find(params[:user_id])
    @collection.destroy
    redirect_to user_collections_path(@user)

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_collection
      @collection = Collection.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def collection_params
      params.require(:collection).permit(:user_id)
    end
end
