class ChannelsController < ApplicationController
  before_action :set_channel, only: [:show]

  def index
    if session[:user_id]
      @user = User.find(session[:user_id])
    end
    @channels = Channel.all
  end

  def show
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_channel
      @channel = Channel.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def channel_params
      params.require(:channel).permit(:user_id)
    end
end
